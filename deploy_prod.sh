#!/usr/bin/env bash
cd src
bin/magento maintenance:enable
composer update
bin/magento setup:upgrade
bin/magento setup:di:compile
bin/magento setup:static-content:deploy it_IT && bin/magento setup:static-content:deploy en_GB && bin/magento setup:static-content:deploy de_DE && bin/magento setup:static-content:deploy fr_FR &&  bin/magento setup:static-content:deploy es_ES
bin/magento cache:flush
bin/magento maintenance:disable
bin/magento setup:static-content:deploy en_US

