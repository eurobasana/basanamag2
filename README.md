#  Magento 2 Docker - Eurostep

###Installazione base di sviluppo progetti magento 2

####Caratteristiche immagine:
Apache 2.4 + PHP 7.0 + OPCache + MariaDB + N98 Magerun 2 + XDebug + Redis


### Requisiti

**MacOS:**

Installare [Docker](https://docs.docker.com/docker-for-mac/install/), [Docker-compose](https://docs.docker.com/compose/install/#install-compose) and [Docker-sync](https://github.com/EugenMayer/docker-sync/wiki/docker-sync-on-OSX).

**Windows:**

Installare [Docker](https://docs.docker.com/docker-for-windows/install/), [Docker-compose](https://docs.docker.com/compose/install/#install-compose) and [Docker-sync](https://github.com/EugenMayer/docker-sync/wiki/docker-sync-on-Windows).

**Linux:**

Installare [Docker](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/) and [Docker-compose](https://docs.docker.com/compose/install/#install-compose).

La procedura di inizializzazione della macchina si distringue in due casi:

- Inizializzazione di un progetto di un nuovo cliente
- Inizializzazione di un progetto esistente a cui non si è mai lavorato nella propria macchina  

#Inizializzazione di un progetto di un nuovo cliente

Clona il progetto nella directory nella quale andrai a repositorare il progetto (Es. /tua-working-copy/tuo-progetto):

```
cd /tua-working-copy/tuo-progetto
git clone https://bitbucket.org/eurostep_it/magento2base.git 
```
Dopo aver scaricato il progetto base, inizializzare il nuovo repository relativo al cliente progetto e cambiare il remote orgin del git repository:

```
cd /tua-working-copy/tuo-progetto
git remote set-url origin https://github.com/eurostep_it/nuovo_progetto.git
```

Eseguire il push di inizializzazione sul nuovo repository:


```
git add .
git commit -m "Inizializzazion progetto"
git push origin master
```

Inizializza la docker image:

```
cd /tua-working-copy/tuo-progetto
./start
```
####Inizializzazione Magento 2

Installa magento 2:

```
cd /tua-working-copy/tuo-progetto
./shell
install-magento2
```

Puoi specificare la versione di magento da installare (e.g. `install-magento2 2.2`). Altrimenti verrà installata l'ultima versione disponibile nel repository di magento.

####Inizializzazione Modulo terze parti ed Eurostep 

Installa magento 2:

```
cd /tua-working-copy/tuo-progetto
./shell
install-composer-modules
```

Verifica l'ip del tuo web server e seleziona l'ip assegnato al container (Es. 172.XXX.XXX.XXX):

```
cd /tua-working-copy/tuo-progetto
./shell
ip addr
```
Imposta nella tua macchina locale il tuo file hosts:

```
vi /etc/hosts
```
Aggiungi al file il record:

```
172.XXX.XXX.XXX dev.local
```


#Inizializzazione di un progetto esistente a cui non si è mai lavorato nella propria macchina  

Clona il progetto  del cliente dal repository bitbucket (Es. /tua-working-copy/nome-cliente):


```
cd /tua-working-copy/nome-cliente
git clone https://bitbucket.org/eurostep_it/nome-cliente.git 
```

Inizializza la docker image:

```
cd /tua-working-copy/nome-cliente
./start
```

Installa magento 2:

```
cd /tua-working-copy/nome-cliente
./shell
init-magento2
```

Se necessario farsi fornire il dump del database e sostituirlo attraverso phpmyadmin.


### Panelli

Accesso alle risorse:

**Magento frontend:** http://dev.local/

**Magento admin:** http://dev.local/admin (user:admin - password:admin123 )

**PHPMyAdmin:** http://localhost:8080 (user:magento - password:magento)

**Local emails:** http://localhost:8025

### Accesso agli strumenti di sviluppo frontend

Acceddi alla sezione di sviluppo frontend 

```
cd /tua-working-copy/tuo-progetto
./shell
cd tools
```
Elimina i file statici esistenti:
```
gulp clean
```
Genera i link simbolici tra cartella pub/static e cartella app/design:
```
gulp deploy
```
Lancia la compilazione del tuo tema:
```
gulp dev --maps
```

A questo punto gulp creerà un server proxy che risponderà sulla porta 3000. 

[dev.local:3000](http://dev.local:3000/)
 
 Sulla porta 3001, sarà a disposizione il pannello di controllo di BrowserSync, nel quale ogni developer può settare le proprie preferenze.
  
 [dev.local:3001](http://dev.local:3001/)

### Comandi

| Commands  | Description  | Options & Examples |
|---|---|---|
| `./start`  | If you continuing not using the CURL you can start your container manually  | |
| `./stop`  | Stop your project containers  | |
| `./kill`  | Stops containers and removes containers, networks, volumes, and images created to the specific project  | |
| `./shell`  | Access your container  | `./shell root` | |
| `./magento`  | Use the power of the Magento CLI  | |
| `./n98`  | Use the Magerun commands as you want | |
| `./xdebug`  |  Enable / Disable the XDebug | |
| `./composer`  |  Use Composer commands | `./composer update` |

#Procedura di deploy in produzione 

- Creare una nuova release attraverso Gitflow (Assegnare un tag corretto a senconda del tipo di rilascio Major - Minor - Patch)
- Pushare il branch Master nel repository remoto
- Accedere a Plesk ed effettuare un pull del branch Master sulla macchina di produzione
- Accedere in ssh alla macchina di produzione
- Lanciare lo script ./deploy_prod.sh
