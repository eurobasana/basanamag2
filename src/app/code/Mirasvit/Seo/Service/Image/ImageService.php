<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-seo
 * @version   2.0.74
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Seo\Service\Image;

use Magento\Catalog\Block\Product\ImageBuilder;
use Mirasvit\Seo\Api\Service\Image\ImageServiceInterface;

class ImageService implements ImageServiceInterface
{
    /**
     * ImageService constructor.
     * @param ImageBuilder $imageBuilder
     */
    public function __construct(
        ImageBuilder $imageBuilder
    ) {
        $this->imageBuilder = $imageBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function getProductBaseImageUrlByProduct($product, $imageId, $attributes = [])
    {
        return $this->imageBuilder->setProduct($product)
            ->setImageId($imageId)
            ->setAttributes($attributes)
            ->create();
    }
}

