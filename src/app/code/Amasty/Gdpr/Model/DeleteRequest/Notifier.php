<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Gdpr
 */


namespace Amasty\Gdpr\Model\DeleteRequest;

use Amasty\Gdpr\Model\Config;
use Magento\Customer\Api\CustomerNameGenerationInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Area;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Mail\Template\TransportBuilder;

class Notifier
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var CustomerNameGenerationInterface
     */
    private $nameGeneration;

    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        Config $config,
        TransportBuilder $transportBuilder,
        CustomerNameGenerationInterface $nameGeneration
    ) {
        $this->customerRepository = $customerRepository;
        $this->config = $config;
        $this->transportBuilder = $transportBuilder;
        $this->nameGeneration = $nameGeneration;
    }

    /**
     * @param $customerId
     * @param $comment
     *
     * @return void
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     */
    public function notify($customerId, $comment)
    {
        try {
            $customer = $this->customerRepository->getById($customerId);
        } catch (NoSuchEntityException $e) {
            return;
        }

        $customerName = $this->nameGeneration->getCustomerName($customer);

        $transport = $this->transportBuilder
            ->setTemplateIdentifier(
                $this->config->getValue('deletion_notification/template')
            )
            ->setTemplateOptions(
                [
                    'area'  => Area::AREA_FRONTEND,
                    'store' => $customer->getStoreId()
                ]
            )
            ->setTemplateVars(
                [
                    'customer' => $customer,
                    'customerName' => $customerName,
                    'comment' => $comment
                ]
            )
            ->setFrom(
                $this->config->getValue('deletion_notification/sender')
            )
            ->addTo(
                $customer->getEmail(),
                $customerName
            )
            ->getTransport();

        $transport->sendMessage();
    }
}
