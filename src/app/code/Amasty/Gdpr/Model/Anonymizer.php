<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Gdpr
 */


namespace Amasty\Gdpr\Model;

use Amasty\Gdpr\Model\ResourceModel\DeleteRequest\CollectionFactory;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\AddressInterface as CustomerAddressInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\Quote\Address as QuoteAddress;
use Magento\Quote\Model\ResourceModel\Quote\Collection as QuoteCollection;
use Magento\Sales\Api\Data\OrderAddressInterface as OrderAddressInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order\Address as OrderAddress;
use Magento\Sales\Model\ResourceModel\Order\Collection as OrderCollection;
use Magento\Store\Model\ScopeInterface;

class Anonymizer
{
    const ANONYMOUS_SYMBOL = '-';

    const RANDOM_LENGTH = 5;

    const EMAIL_TEMPLATE_CODE = 'amgdpr_anonimization';

    /**
     * @var \Magento\Framework\Event\Manager
     */
    private $eventManager;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * @var \Magento\Quote\Model\ResourceModel\Quote\CollectionFactory
     */
    private $quoteCollectionFactory;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    private $customerCollectionFactory;

    /**
     * @var CustomerData
     */
    private $customerData;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var \Magento\Newsletter\Model\ResourceModel\Subscriber
     */
    private $subscriberResource;

    /**
     * @var \Magento\Newsletter\Model\Subscriber
     */
    private $subscriber;

    private $isDeleting = false;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var CollectionFactory
     */
    private $deleteRequestCollectionFactory;

    /**
     * @var ActionLogger
     */
    private $logger;

    public function __construct(
        \Magento\Framework\Event\Manager $eventManager,
        CustomerRepositoryInterface $customerRepository,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Quote\Model\ResourceModel\Quote\CollectionFactory $quoteCollectionFactory,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        CustomerData $customerData,
        OrderRepositoryInterface $orderRepository,
        CartRepositoryInterface $quoteRepository,
        \Magento\Newsletter\Model\Subscriber $subscriber,
        AddressRepositoryInterface $addressRepository,
        \Magento\Newsletter\Model\ResourceModel\Subscriber $subscriberResource,
        TransportBuilder $transportBuilder,
        ScopeConfigInterface $scopeConfig,
        CollectionFactory $deleteRequestCollectionFactory,
        ActionLogger $logger
    ) {
        $this->eventManager = $eventManager;
        $this->customerRepository = $customerRepository;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->quoteCollectionFactory = $quoteCollectionFactory;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->customerData = $customerData;
        $this->orderRepository = $orderRepository;
        $this->quoteRepository = $quoteRepository;
        $this->subscriber = $subscriber;
        $this->addressRepository = $addressRepository;
        $this->subscriberResource = $subscriberResource;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->deleteRequestCollectionFactory = $deleteRequestCollectionFactory;
        $this->logger = $logger;
    }

    /**
     * @param string|int $customerId
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function deleteCustomer($customerId)
    {
        $this->isDeleting = true;
        $this->anonymizeCustomer($customerId);

        $this->logger->logAction('delete_request_approved', $customerId);
    }

    /**
     * @param $customerId
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     */
    public function anonymizeCustomer($customerId)
    {
        $this->eventManager->dispatch(
            'before_amgdpr_customer_anonymisation',
            ['customerId' => $customerId, 'isDeleting' => $this->isDeleting]
        );

        $this->anonymizeOrders($customerId);
        $this->anonymizeQuotes($customerId);
        $this->deleteSubscription($customerId);
        $this->anonymizeAccountInformation($customerId);

        if (!$this->isDeleting) {
            $this->logger->logAction('data_anonymised_by_customer', $customerId);
        }

        $this->eventManager->dispatch(
            'after_amgdpr_customer_anonymisation',
            ['customerId' => $customerId, 'isDeleting' => $this->isDeleting]
        );
    }

    /**
     * @param int|string $customerId
     */
    private function anonymizeOrders($customerId)
    {
        /** @var OrderCollection $entities */
        $orders = $this->orderCollectionFactory->create();

        $orders->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', $customerId);

        /** @var \Magento\Sales\Model\Order $item */
        foreach ($orders as $item) {
            $this->prepareSalesData($item);
            //@codingStandardsIgnoreStart
            $this->orderRepository->save($item);
            //@codingStandardsIgnoreEnd
        }
    }

    /**
     * @param \Magento\Quote\Model\Quote|\Magento\Sales\Model\Order $object
     */
    private function prepareSalesData($object)
    {
        $object->setCustomerFirstname($this->generateFieldValue());
        $object->setCustomerMiddlename($this->generateFieldValue());
        $object->setCustomerLastname($this->generateFieldValue());
        $object->setCustomerEmail($this->getRandomEmail());
        if ($object->getBillingAddress()) {
            $this->anonymizeAddress($object->getBillingAddress());
        }
        if ($object->getShippingAddress()) {
            $this->anonymizeAddress($object->getShippingAddress());
        }
    }

    /**
     * @return string
     */
    public function generateFieldValue()
    {
        $rand = self::ANONYMOUS_SYMBOL;
        if (!$this->isDeleting) {
            $rand = 'anonymous' . $this->getRandomString();
        }

        return $rand;
    }

    /**
     * @return string
     */
    private function getRandomString()
    {
        return bin2hex(openssl_random_pseudo_bytes(self::RANDOM_LENGTH));
    }

    /**
     * @return string
     */
    public function getRandomEmail()
    {
        $email = self::ANONYMOUS_SYMBOL;
        if (!$this->isDeleting) {
            $email = $this->generateFieldValue();
        }
        $email = $email . '@' . $this->getRandomString() . '.com';

        if ($this->isEmailExists($email)) {
            $email = $this->getRandomEmail();
        }

        return $email;
    }

    public function isEmailExists($email)
    {
        $collection = $this->customerCollectionFactory->create();

        return (bool)$collection->addFieldToFilter('email', $email)->getSize();
    }

    /**
     * @param OrderAddress|QuoteAddress|OrderAddressInterface|CustomerAddressInterface|null $address
     */
    private function anonymizeAddress($address)
    {
        $attributeCodes = $this->customerData->getAttributeCodes('customer_address');

        foreach ($attributeCodes as $code) {
            switch ($code) {
                case 'telephone':
                case 'fax':
                    $randomString = '0000000';
                    break;
                case 'country_id':
                case 'region':
                    continue 2;
                default:
                    $randomString = $this->generateFieldValue();
            }
            $address->setData($code, $randomString);
        }
    }

    /**
     * @param int|string $customerId
     */
    private function anonymizeQuotes($customerId)
    {
        /** @var QuoteCollection $entities */
        $quotes = $this->quoteCollectionFactory->create();

        $quotes->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', $customerId);

        /** @var \Magento\Quote\Model\Quote $item */
        foreach ($quotes as $item) {
            $this->prepareSalesData($item);
            //@codingStandardsIgnoreStart
            $this->quoteRepository->save($item);
            //@codingStandardsIgnoreEnd
        }
    }

    /**
     * @param int|string $customerId
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteSubscription($customerId)
    {
        /** @var \Magento\Newsletter\Model\Subscriber $subscriber */
        $subscriber = $this->subscriber->loadByCustomerId($customerId);
        if ($subscriber->getId()) {
            $subscriber->unsubscribe();
            $this->subscriberResource->delete($subscriber);
        }
    }

    /**
     * @param int|string $customerId
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     */
    public function anonymizeAccountInformation($customerId)
    {
        /** @var \Magento\Customer\Model\Data\Customer $customer */
        $customer = $this->customerRepository->getById($customerId);
        $oldEmail = $customer->getEmail();

        $attributeCodes = $this->customerData->getAttributeCodes('customer');

        foreach ($attributeCodes as $attributeCode) {
            switch ($attributeCode) {
                case 'email':
                    $randomString = $this->getRandomEmail();
                    break;
                case 'dob':
                case 'gender':
                    $randomString = '';
                    break;
                default:
                    $randomString = $this->generateFieldValue();
            }
            $customer->setData($attributeCode, $randomString);
        }

        if (!$this->isDeleting) {
            $this->sendConfirmationEmail($oldEmail, $customer);
            $this->deleteRequestCollectionFactory->create()->deleteByCustomerId($customer->getId());
        }
        $this->customerRepository->save($customer);

        $addresses = $customer->getAddresses();
        /** @var \Magento\Customer\Api\Data\AddressInterface $address */
        foreach ($addresses as $address) {
            $this->anonymizeAddress($address);
            //@codingStandardsIgnoreStart
            $this->addressRepository->save($address);
            //@codingStandardsIgnoreEnd
        }
    }

    /**
     * @param string                                $realEmail
     * @param \Magento\Customer\Model\Data\Customer $customer
     *
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendConfirmationEmail($realEmail, $customer)
    {
        $template = $this->scopeConfig->getValue(
            'amasty_gdpr/anonymisation_notification/template',
            ScopeInterface::SCOPE_STORE
        );

        $sender = $this->scopeConfig->getValue(
            'amasty_gdpr/anonymisation_notification/sender',
            ScopeInterface::SCOPE_STORE
        );

        $transport = $this->transportBuilder->setTemplateIdentifier(
            $template
        )->setTemplateOptions(
            [
                'area'  => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $customer->getStoreId()
            ]
        )->setTemplateVars(
            ['anonymousEmail' => $customer->getEmail()]
        )->setFrom(
            $sender
        )->addTo(
            $realEmail,
            $customer->getFirstname() . ' ' . $customer->getLastname()
        )->getTransport();

        $transport->sendMessage();
    }
}
