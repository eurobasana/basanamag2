<?php

/**
 * Eurostep
 * Module to allow all country list in MageDelight_Storelocator module Store Address backend configuration
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Eurostep_StorelocatorEs',
    __DIR__
);
