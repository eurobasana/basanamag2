<?php

/**
 * Eurostep
 * Module to allow all country list in MageDelight_Storelocator module Store Address backend configuration
 */

namespace Eurostep\StorelocatorEs\Block\Adminhtml;

use \Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magedelight\Storelocator\Block\Adminhtml\Country;

class Countryes extends \Magedelight\Storelocator\Block\Adminhtml\Country

{
    public function getCountryCollection()
    {
        $collection = $this->getData('country_collection');
        if ($collection === null) {
            $collection = $this->_countryCollectionFactory->create()->load();
            $this->setData('country_collection', $collection);
        }

        return $collection;
    }
}
