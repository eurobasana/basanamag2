<?php

namespace Eurostep\CoverImage\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Coverimage extends Template implements BlockInterface
{

    protected $_template = "widget/coverimage.phtml";

}
