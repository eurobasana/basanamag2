<?php
namespace Eurostep\BaseHelper\Helper;

use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Catalog\Helper\Image;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /** @var ProductFactory */
    protected $_productFactory;

    /** @var Image */
    protected $_imageHelper;


    public function __construct(
        Context $context,
        Image $imageHelper,
        ProductFactory $productModel
    ) {
        parent::__construct($context);
        $this->_productFactory = $productModel;
        $this->_imageHelper = $imageHelper;
    }

    public function getHoverImage($product, $imgType)
    {
        $_product = $this->_productFactory->create()->load($product->getId());
        $mediaGallery = $_product->getMediaGalleryImages();
        $count = 0;
        foreach ($mediaGallery as $image){
            if($count == 1 && $image->getUrl() != null){
                $newImage = $this->_imageHelper->init($product, $imgType)
                    ->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)
                    ->setImageFile($image->getFile());
                return $newImage;
            }
            $count++;
        }
    }
}