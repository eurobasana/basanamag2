<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 18/06/18
 * Time: 16.45
 */

namespace Eurostep\CustomRelated\Block\Products\Related;

use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\View\Element\AbstractBlock;

class ColorsList extends \Magento\Catalog\Block\Product\AbstractProduct //implements \Magento\Framework\DataObject\IdentityInterface
{
    /**
     * @var Collection
     */
    protected $_itemCollection;


    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $_catalogProductVisibility;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Swatches\Helper\Media
     */
    protected $_swatchesMediaHelper;

    /**
     * @var \Magento\Swatches\Helper\Data
     */
    protected $_swatchHelper;

    /**
     * @var \Magento\Swatches\Helper\Media
     */
    protected $_eavConfig;


    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Swatches\Helper\Media $swatchMedia
     * @param \Magento\Swatches\Helper\Data $swatchHelper
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Swatches\Helper\Media $swatchMedia,
        \Magento\Swatches\Helper\Data $swatchHelper,
        \Magento\Eav\Model\Config $eavConfig,
        array $data = []
    ) {
        $this->_catalogProductVisibility = $catalogProductVisibility;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_swatchesMediaHelper = $swatchMedia;
        $this->_swatchHelper = $swatchHelper;
        $this->_eavConfig = $eavConfig;
        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * @return $this
     */
    protected function _prepareData()
    {
        $product = $this->_coreRegistry->registry('product');
        /* @var $product \Magento\Catalog\Model\Product */
        $productIds = [];

        $collection = $product->getRelatedProductCollection()->addStoreFilter();
        $collection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());
        $collection->load();

        foreach ($collection as $item) {
            $item->setDoNotUseCategoryId(true);
            $productIds[] = $item->getId();
        }
        $productIds[] = $product->getId();

        $this->_itemCollection = $this->_productCollectionFactory->create()
            ->addFieldToFilter('entity_id',['in' => $productIds])
            ->addAttributeToSelect('*')
            ->load();

        return $this;
    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $this->_prepareData();
        return parent::_beforeToHtml();
    }

    /**
     * @return Collection
     */
    public function getItems()
    {
        /**
         * getIdentities() depends on _itemCollection populated, but it can be empty if the block is hidden
         * @see https://github.com/magento/magento2/issues/5897
         */
        if ($this->_itemCollection === null) {
            $this->_prepareData();
        }
        return $this->_itemCollection;
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        $identities = [];
        foreach ($this->getItems() as $item) {
            $identities = array_merge($identities, $item->getIdentities());
        }
        return $identities;
    }

    /**
     * @param $swatchId
     * @return array
     */
    public function getProductSwatchData($swatchId)
    {
        $attributeOptions = $this->_eavConfig->getAttribute('catalog_product', 'color')->getSource()->getAllOptions();

        //Get all value_id's from attribute and add them to a array
        $valueIds = [];
        foreach ($attributeOptions as $option) {
            if($option['value'] == $swatchId){
                $valueIds[] = $option['value'];
                break;
            }
        }
        $swatch = $this->_swatchHelper->getSwatchesByOptionsId($valueIds);
        return $swatch;
    }

    /**
     * @param $swatchId
     * @return string
     */
    public function getSwatchValue($swatchId)
    {
        $swatchOption = $this->getProductSwatchData($swatchId);
        if($swatchOption){
            if(preg_match('/^#[a-f0-9]{6}$/i', $swatchOption[$swatchId]['value'])){
                $html = '<span class="ec-related-swatch-wrapper"><span class="ec-related-swatch-color" style="background-color: ' . $swatchOption[$swatchId]['value'] . '"></span></span>';
            }elseif (strstr($swatchOption[$swatchId]['value'], '/')){
                $bgImage = $this->_swatchesMediaHelper->getSwatchAttributeImage('swatch_image', $swatchOption[$swatchId]['value']);
                $html = '<span class="ec-related-swatch-wrapper"><span class="ec-related-swatch-image" style="background: url(' . $bgImage . ') no-repeat center; background-size: initial;"></span></span>';
            }else{
                $html = '<span class="ec-related-swatch-wrapper"><span class="ec-related-swatch-color ec-no-color" style="background-color: #fdfdfd"></span></span>';
            }
        }else{
            $html = '<span class="ec-related-swatch-wrapper"><span class="ec-related-swatch-color ec-color-error" style="background-color: #cccccc; border: 1px solid red"></span></span>';
        }
        return $html;
    }
}