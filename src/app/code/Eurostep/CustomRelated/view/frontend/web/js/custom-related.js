/**
 * Created by sergiu on 20/06/18.
 */
define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('eurostep.customRelatedJs', {
        options: {
            wrapper: '',
            timer: null
        },

        _create: function () {
            this._setActiveLabel();
            this._bindMouseEvent();
        },

        _setActiveLabel: function () {
            var activeLabel = $(this.element).find('.ec-swatch-text.ec-active').html();
            $(this.options.wrapper).html(activeLabel);
        },

        _bindMouseEvent: function () {
            var self = this;
            this._on({
                'mouseenter .ec-related-colors-list > li > a:not(.ec-active-swatch)': function (e) {
                    if(self.options.timer){
                        //noinspection JSCheckFunctionSignatures
                        clearTimeout(self.options.timer);
                        self.options.timer = null;
                    }
                    var label = $(e.currentTarget).find('.ec-swatch-text').html();
                    $(self.options.wrapper).html(label);
                },

                'mouseleave .ec-related-colors-list > li > a:not(.ec-active-swatch)': function (e) {
                    self.options.timer = setTimeout(function () {
                        self._setActiveLabel();
                    }, 1000);
                }
            })
        }

    });

    return $.eurostep.customRelatedJs;
});