<?php

namespace Eurostep\CashOnDeliveryToProcessing\Observer;

use Magento\Framework\Event\ObserverInterface;

class SalesOrderSaveAfter implements ObserverInterface
{

    protected $connector; public function __construct()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {

        /**
         * @var $order  \Magento\Sales\Model\Order
         */
        $order = $observer->getEvent()->getOrder();
        $method = $order->getPayment()->getMethod();
        $state = $order->getState();

        if ($method === "msp_cashondelivery" && $state === "new"){
            $order->setState('processing')->addStatusToHistory("processing","Moved to processing");
            $order->save();
        }


    }
}