<?php
/**
 * Created by PhpStorm.
 * User: sergiu
 * Date: 04/07/18
 * Time: 9.58
 */

namespace Eurostep\DateOfBirthGDPR\Block\Widget;

use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Helper\Address;
use Magento\Framework\View\Element\Html\Date;
use Magento\Framework\Data\Form\FilterFactory;

class Dob extends \Magento\Customer\Block\Widget\Dob
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Helper\Address $addressHelper
     * @param CustomerMetadataInterface $customerMetadata
     * @param \Magento\Framework\View\Element\Html\Date $dateElement
     * @param \Magento\Framework\Data\Form\FilterFactory $filterFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Address $addressHelper,
        CustomerMetadataInterface $customerMetadata,
        Date $dateElement,
        FilterFactory $filterFactory,
        array $data = []
    )
    {
        parent::__construct($context, $addressHelper, $customerMetadata, $dateElement, $filterFactory, $data);
    }

    /**
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('Magento_Customer::widget/dob.phtml');
    }

    /**
     * Return data-validate rules
     *
     * @return string
     */
    public function getHtmlExtraParams()
    {
        $html = '';
        $validators = [];

        if ($this->isRequired()) {
            $validators['required'] = true;
        }

        $validators['validate-minimum-age'] =[
            'dateFormat' => $this->getDateFormat()
        ];
        $validators['validate-date'] = [
            'dateFormat' => $this->getDateFormat()
        ];
        //$label = __('Date of Birth');
        //$html .= 'placeholder="'.$label.'" ';
        $html .= 'data-validate="' . $this->_escaper->escapeHtml(json_encode($validators)) . '"';

        return $html;
    }
}