/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 *
 */

var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/action/set-shipping-information': {
                'Eurostep_DateOfBirthGDPR/js/action/set-shipping-information-mixin': true
            },
            'Magento_Ui/js/lib/validation/rules': {
                'Eurostep_DateOfBirthGDPR/js/mage-ui-validation': true
            }
        }
    },
    map: {
        "*": {
            'mage/validation': "Eurostep_DateOfBirthGDPR/js/mage-validation",
            'mage/validation/validation': 'mage/validation/validation'
        }
    }
};