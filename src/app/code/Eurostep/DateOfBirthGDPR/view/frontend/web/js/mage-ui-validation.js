/**
 * Created by sergiu on 28/06/18.
 */
define(['mage/translate'], function($t) {
    'use strict';

    return function(rules) {
        rules['validate-minimum-age'] = {
            handler: function (value) {
                var birthday = +new Date(value);
                return~~ ((Date.now() - birthday) / (31557600000)) > 15;
            },
            message: $t('You must be 16 years of age or older to register on our website')
        };
        return rules;
    };
});