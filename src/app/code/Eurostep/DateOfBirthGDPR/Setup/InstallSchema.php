<?php

namespace Eurostep\DateOfBirthGDPR\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    protected $logger;

    function __construct(
      \Psr\Log\LoggerInterface $loggerInterface
    ) {
        $this->logger = $loggerInterface;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
        $connection = $setup->getConnection();

        // Add `dob` field into `quote_address` table
        if( $connection->isTableExists('quote_address') &&
            !$connection->tableColumnExists('quote_address', 'dob'))
        {
            $connection->addColumn(
                $setup->getTable('quote_address'),
                'dob',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                    'nullable' => false,
                    'default' => false,
                    'comment' => 'Customer date of birth'
                ]
            );
        }

        $setup->endSetup();
    }
}