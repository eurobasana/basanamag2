<?php
namespace Eurostep\DateOfBirthGDPR\Plugin\Checkout;

class GuestPaymentInformationManagement {

    protected $quoteIdMaskFactory;

    protected $quoteRepository;

    protected $order;

    public function __construct(
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Magento\Sales\Model\Order $order
    )
    {
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->quoteRepository = $quoteRepository;
        $this->order = $order;
    }

    public function beforeSavePaymentInformation(
        \Magento\Checkout\Model\GuestPaymentInformationManagement $subject,
        $cartId,
        $email,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        $extAttributes = $billingAddress->getExtensionAttributes();

        if(!empty($extAttributes)):
            $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
            $quote = $this->quoteRepository->getActive($quoteIdMask->getQuoteId());
            $dob = $extAttributes->getDobCheckout();
            $quote->setCustomerDob($dob);
            $quote->getShippingAddress()->setDob($dob);
            $quote->getBillingAddress()->setDob($dob);
        endif;
    }
}