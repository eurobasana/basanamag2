<?php

namespace Eurostep\DateOfBirthGDPR\Plugin\Checkout;

class GuestShippingInformationManagement
{
    protected $quoteIdMaskFactory;

    protected $quoteRepository;

    public function __construct(
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory,
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    )
    {
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * Save/update `invoice_request` flag value into the quote
     *
     * @param \Magento\Checkout\Model\GuestShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\GuestShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $extAttributes = $addressInformation->getShippingAddress()->getExtensionAttributes();

        if(!empty($extAttributes)):
            $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
            $quote = $this->quoteRepository->getActive($quoteIdMask->getQuoteId());
            $dob = $extAttributes->getDobCheckout();
            $quote->setCustomerDob($dob);
            $quote->getShippingAddress()->setDob($dob);
            $quote->getBillingAddress()->setDob($dob);
        endif;
    }
}