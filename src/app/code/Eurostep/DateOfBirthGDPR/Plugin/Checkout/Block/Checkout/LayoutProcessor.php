<?php
namespace Eurostep\DateOfBirthGDPR\Plugin\Checkout\Block\Checkout;

class LayoutProcessor
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;


    /**
     * Plugin constructor.
     *
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_customerSession = $customerSession;
    }

    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {
        if(!$this->checkHasDob()){
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']['children']['dob_checkout'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress.custom_attributes',
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'ui/form/element/date',
                    'options' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange'=> '-90:+0',
                    ],
                    'id' => 'dob_checkout'
                ],
                'dataScope' => 'shippingAddress.custom_attributes.dob_checkout',
                'label' => __('Date of Birth'),
                'provider' => 'checkoutProvider',
                'visible' => true,
                'validation' => [
                    'required-entry' => true,
                    'validate-minimum-age'=> true
                ],
                'sortOrder' => 100,
                'id' => 'dob_checkout'
            ];
        }

        return $jsLayout;
    }

    public function checkHasDob()
    {
        if($this->_customerSession->isLoggedIn())
        {
            return (bool)$this->_customerSession->getCustomer()->getDob();
        }
        return false;
    }

}