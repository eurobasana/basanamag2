<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 11/07/18
 * Time: 11.53
 */

namespace Eurostep\CustomInvoice\Preference\Sales\Controller\Adminhtml\Order\Invoice;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class PrintAction extends \Magento\Sales\Controller\Adminhtml\Order\Invoice\PrintAction
{

    /**
     * @return ResponseInterface|void
     */
    public function execute()
    {
        $invoiceId = $this->getRequest()->getParam('invoice_id');
        if ($invoiceId) {
            $invoice = $this->_objectManager->create(
                \Magento\Sales\Api\InvoiceRepositoryInterface::class
            )->get($invoiceId);
            $orderId = $invoice->getOrder()->getIncrementId();
            if ($invoice) {
                $pdf = $this->_objectManager->create(\Magento\Sales\Model\Order\Pdf\Invoice::class)->getPdf([$invoice]);
                $date = $this->_objectManager->get(
                    \Magento\Framework\Stdlib\DateTime\DateTime::class
                )->date('Y-m-d_H-i-s');
                return $this->_fileFactory->create(
                    $orderId.'-0_receipt_' . $date . '.pdf',
                    $pdf->render(),
                    DirectoryList::VAR_DIR,
                    'application/pdf'
                );
            }
        } else {
            return $this->resultForwardFactory->create()->forward('noroute');
        }
    }

}