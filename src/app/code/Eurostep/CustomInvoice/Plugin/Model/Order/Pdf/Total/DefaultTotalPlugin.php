<?php
/**
 * Created by PhpStorm.
 * User: alberto
 * Date: 23/04/18
 * Time: 18.18
 */

namespace Eurostep\CustomInvoice\Plugin\Model\Order\Pdf\Total;


class DefaultTotalPlugin
{
    public function beforeGetTotalsForDisplay(\Magento\Sales\Model\Order\Pdf\Total\DefaultTotal $subject)
    {
        $field = $subject->getSourceField();
        if($field == 'grand_total'){
            $subject->setTitle(__('Grand Total') . ' ' . __('(tax included)'));
        }
    }

}