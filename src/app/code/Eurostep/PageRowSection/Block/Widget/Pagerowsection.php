<?php

namespace Eurostep\PageRowSection\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Pagerowsection extends Template implements BlockInterface
{

    protected $_template = "widget/pagerowsection.phtml";

    /**
     * Return button link attributes depends on type of link
     *
     * @return string
     */
    public function getLinkAttributes()
    {
        $linkData = $this->getData('page_section_button_link');
        if(strpos($linkData, 'http') !== false)
            return 'href="' . $linkData . '" target="_blank" title="' . $this->getBlockTitle() . '"';
        else
            return 'href="' . $this->getBaseUrl() . $linkData . '" title="' . $this->getBlockTitle() . '"';
    }

    /**
     * @return mixed
     */
    public function getButtonLabel()
    {
        return $this->getData('page_section_button_label');
    }

    /**
     * @return mixed
     */
    public function getBlockTitle()
    {
        return $this->getData('page_section_title');
    }

}
