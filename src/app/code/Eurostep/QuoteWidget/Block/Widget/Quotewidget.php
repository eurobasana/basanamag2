<?php

namespace Eurostep\QuoteWidget\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Quotewidget extends Template implements BlockInterface
{

    protected $_template = "widget/quotewidget.phtml";

}
