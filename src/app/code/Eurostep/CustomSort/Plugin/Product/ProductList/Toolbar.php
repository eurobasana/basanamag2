<?php
namespace Eurostep\CustomSort\Plugin\Product\ProductList;

class Toolbar
{

     /**
      * Plugin
      *
      * @param \Magento\Catalog\Block\Product\ProductList\Toolbar $subject
      * @param \Closure $proceed
      * @param \Magento\Framework\Data\Collection $collection
      * @return \Magento\Catalog\Block\Product\ProductList\Toolbar
      */
     public function aroundSetCollection(
             \Magento\Catalog\Block\Product\ProductList\Toolbar $toolbar,
             \Closure $proceed,
             $collection
     ) {
         $currentOrder = $toolbar->getCurrentOrder();
         $result = $proceed($collection);

         if ($currentOrder) {
             switch ($toolbar->getCurrentOrder()) {

                 case 'created_at':

                     if ( $toolbar->getCurrentDirection() == 'asc' ) {

                         $toolbar->getCollection()
                                 ->getSelect()
                                 ->order('e.created_at DESC');


                     } elseif ( $toolbar->getCurrentDirection() == 'desc' ) {

                         $toolbar->getCollection()
                                 ->getSelect()
                                 ->order('e.created_at ASC');

                     }

                     break;

                 case 'name_desc':
                     $toolbar->getCollection()
                             ->setOrder('name', 'desc');
                     break;

                 case 'name_asc':
                     $toolbar->getCollection()
                             ->setOrder('name', 'asc');
                     break;

                 case 'price_desc':
                     $toolbar->getCollection()
                             ->setOrder('price', 'desc');
                     break;

                 case 'price_asc':
                     $toolbar->getCollection()
                             ->setOrder('price', 'asc');
                     break;


                 default:

                   $toolbar->getCollection()->setOrder($toolbar->getCurrentOrder(), $toolbar->getCurrentDirection());

                     break;

             }
         }

         return $toolbar;
     }
}