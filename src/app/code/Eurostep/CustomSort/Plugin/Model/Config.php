<?php
namespace Eurostep\CustomSort\Plugin\Model;

class Config
{

    /**
     * Adding custom options and changing labels
     *
     * @param \Magento\Catalog\Model\Config $catalogConfig
     * @param [] $options
     * @return []
     */
    public function afterGetAttributeUsedForSortByArray(\Magento\Catalog\Model\Config $catalogConfig, $options)
    {

        //Remove specific default sorting options(if require)
        unset($options['position']);
        unset($options['name']);
        unset($options['price']);

        //Changing label(if require)
//        $newOption['position'] = __('Any Thing');

        //New sorting options
        $newOption['price_desc'] = __('Price High to Low');
        $newOption['price_asc'] = __('Price Low to High');
        $newOption['name_asc'] = __('A-Z');
        $newOption['name_desc'] = __('Z-A');
        $newOption['created_at'] = __('New Products');

        //Merge default sorting options with new options
        $options = array_merge($newOption, $options);

        return $options;
    }
}