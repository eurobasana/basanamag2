<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Catalog Product List Sortable allowed sortable attributes source
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
namespace Eurostep\CustomSort\Model\Config\Source;

class ListSort implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Catalog config
     *
     * @var \Magento\Catalog\Model\Config
     */
    protected $_catalogConfig;

    /**
     * Construct
     *
     * @param \Magento\Catalog\Model\Config $catalogConfig
     */
    public function __construct(\Magento\Catalog\Model\Config $catalogConfig)
    {
        $this->_catalogConfig = $catalogConfig;
    }

    /**
     * Retrieve option values array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];

        $options[] = ['label' => __('Price Low to High'), 'value' => 'price_asc'];
        $options[] = ['label' => __('Price High to Low'), 'value' => 'price_desc'];
        $options[] = ['label' => __('A-Z'), 'value' => 'name_asc'];
        $options[] = ['label' => __('Z-A'), 'value' => 'name_desc'];
        $options[] = ['label' => __('New Products'), 'value' => 'created_at'];


      foreach ($this->_getCatalogConfig()->getAttributesUsedForSortBy() as $attribute) {
            $options[] = ['label' => __($attribute['frontend_label']), 'value' => $attribute['attribute_code']];
        }


        //Remove original magento catalog module options

        for ($i = 0; $i < 2; $i++) {
            if (($key = array_search('price', array_column($options, 'value'))) || ($key = array_search('name', array_column($options, 'value')))) {
                unset($options[$key]);
            }
        }

        return $options;
    }

    /**
     * Retrieve Catalog Config Singleton
     *
     * @return \Magento\Catalog\Model\Config
     */
    protected function _getCatalogConfig()
    {
        return $this->_catalogConfig;
    }
}
