<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */
namespace Nostress\Koongo\Block\Adminhtml\Category\Checkboxes;

/**
 * Categories tree with checkboxes
 *
 * @author     Nostress Team <info@nostresscommerce.c>
 */

use Magento\Framework\Data\Tree\Node;

class Tree extends \Magento\Catalog\Block\Adminhtml\Category\Checkboxes\Tree
{
	/**
	 * Tree representation as php array
	 * @var unknown_type
	 */
	protected $treeRootArray;
	
	/**
	 * Category info indexed array
	 * @var unknown_type
	 */
	protected $categoryIndexedArray;
	
    /**
     * @return void
     */
    protected function _prepareLayout()
    {
        $this->setTemplate('Nostress_Koongo::catalog/category/checkboxes/tree.phtml');
    }
    
    /**
     * @param mixed|null $parenNodeCategory
     * @return string
     */
    public function getTreeJson($parenNodeCategory = null)
    {    	
    	$rootArray = $this->getTreeRootArray($parenNodeCategory);
    	$json = $this->_jsonEncoder->encode(isset($rootArray['children']) ? $rootArray['children'] : []);
    	return $json;
    }
    
    protected function getTreeRootArray($parenNodeCategory = null)
    {
    	if(!isset($this->treeRootArray))
    		$this->treeRootArray = $this->_getNodeJson($this->getRoot($parenNodeCategory));
    	return $this->treeRootArray;    		
    }
    
    public function getCategoriesInfoJson()
    {
    	$this->categoryIndexedArray = [];
    	$node = $this->getTreeRootArray();
    	if(!empty($node['children']))
    	{
    		//Skip default category if it is just one
    		if(count($node['children']) == 1)
    		{    			
    			$node = $node['children'][0];
    			//Save Default category
    			$name = htmlspecialchars_decode($node['name']);
    			$this->categoryIndexedArray[(int)$node['id']] = ['name' => $name,'path' => $name];
    		}
    			
    		if(!empty($node['children']))
    		{    		
    			foreach($node['children'] as $childItem)
    			{
    				$this->populateCategoriesIndexedArray("",$childItem);    	
    			}
    		}
    	}
    	return json_encode($this->categoryIndexedArray);

    	
    }
    
    protected function populateCategoriesIndexedArray($parentPath = null,$item)
    {    	    	 
    	$name = $path = htmlspecialchars_decode($item['name']);
    	
    	if(!empty($parentPath))
    	{
    		$path = $parentPath.' > '.$path;
    	}
  	    	    	
    	$this->categoryIndexedArray[(int)$item['id']] = ['name' => $name,'path' => $path];
    	
    	if(!empty($item['children']))
    	{
    		foreach($item['children'] as $childItem)
    		{
    			$this->populateCategoriesIndexedArray($path,$childItem);    			
    		}
    	}
    }
    
    /**
     * @param array|Node $node
     * @param int $level
     * @return array
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _getNodeJson($node, $level = 1)
    {
    	$item = parent::_getNodeJson($node, $level);
    	$item['name'] = $this->escapeHtml($node->getName());
    	return $item;
    }
}
