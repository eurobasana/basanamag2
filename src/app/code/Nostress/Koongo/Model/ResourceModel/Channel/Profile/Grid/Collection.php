<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 * ResourceModel for Koongo Connector profiles grid collection
 *
 * @category Nostress
 * @package Nostress_Koongo
 *
 */

namespace Nostress\Koongo\Model\ResourceModel\Channel\Profile\Grid;

use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Search\AggregationInterface;
use Nostress\Koongo\Model\ResourceModel\Channel\Profile\Collection as ProfileCollection;

/**
 * Class Collection
 * Collection for displaying grid of sales documents
 */
class Collection extends ProfileCollection implements SearchResultInterface
{
    /**
     * @var AggregationInterface
     */
    protected $aggregations;

    /**
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param mixed|null $mainTable
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb $eventPrefix
     * @param mixed $eventObject
     * @param mixed $resourceModel
     * @param string $model
     * @param null $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb|null $resource
     * 
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        $mainTable,
        $eventPrefix,
        $eventObject,
        $resourceModel,
        $model = 'Magento\Framework\View\Element\UiComponent\DataProvider\Document',
        $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {                     
        parent::__construct(
        		$entityFactory,
        		$logger,
        		$fetchStrategy,
        		$eventManager,
        		$connection,
        		$resource
        );
        $this->_eventPrefix = $eventPrefix;
        $this->_eventObject = $eventObject;
        $this->_init($model, $resourceModel);
        $this->setMainTable($mainTable);
        $this->joinFeedTable();
    }
    
    public function joinFeedTable()
    {
    	$table = $this->getResource()->getTable('nostress_koongo_channel_feed');
    	$cond = "{$table}.code = main_table.feed_code";
    	$cols = [ 'link','channel_code','type','file_type','taxonomy_code'];
    	$this->join($table, $cond, $cols);
    }
    
    /**
     * @return AggregationInterface
     */
    public function getAggregations()
    {
        return $this->aggregations;
    }

    /**
     * @param AggregationInterface $aggregations
     * @return $this
     */
    public function setAggregations($aggregations)
    {
        $this->aggregations = $aggregations;
    }


    /**
     * Retrieve all ids for collection
     * Backward compatibility with EAV collection
     *
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getAllIds($limit = null, $offset = null)
    {
    	$idsSelect = $this->_getAllIdsSelect($limit, $offset);
    	
    	//Add main_table to where part
    	$wherePart = $idsSelect->getPart(\Magento\Framework\DB\Select::WHERE);
    	$idsSelect->reset(\Magento\Framework\DB\Select::WHERE);
    	foreach($wherePart as $key => $part)
    	{
    		$idsSelect->where(str_replace("`entity_id`", "main_table.entity_id", $part));
    		
    	}
    	//$idsSelect->where($wherePart);
    	//echo $idsSelect->__toString(); exit();
        return $this->getConnection()->fetchCol($idsSelect, $this->_bindParams);
    }
    
    /**
     * Create all ids retrieving select with limitation
     * Backward compatibility with EAV collection
     *
     * @param int $limit
     * @param int $offset
     * @return \Magento\Eav\Model\Entity\Collection\AbstractCollection
     */
    protected function _getAllIdsSelect($limit = null, $offset = null)
    {
    	$idsSelect = clone $this->getSelect();
    	$idsSelect->reset(\Magento\Framework\DB\Select::ORDER);
    	$idsSelect->reset(\Magento\Framework\DB\Select::LIMIT_COUNT);
    	$idsSelect->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET);
    	$idsSelect->reset(\Magento\Framework\DB\Select::COLUMNS);
    	
    	$idsSelect->columns("main_table.".$this->getResource()->getIdFieldName(), 'main_table');
    	$idsSelect->limit($limit, $offset);
    	return $idsSelect;
    }

    /**
     * Get search criteria.
     *
     * @return \Magento\Framework\Api\SearchCriteriaInterface|null
     */
    public function getSearchCriteria()
    {
        return null;
    }

    /**
     * Set search criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setSearchCriteria(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null)
    {
        return $this;
    }

    /**
     * Get total count.
     *
     * @return int
     */
    public function getTotalCount()
    {
        return $this->getSize();
    }

    /**
     * Set total count.
     *
     * @param int $totalCount
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setTotalCount($totalCount)
    {
        return $this;
    }

    /**
     * Set items list.
     *
     * @param \Magento\Framework\Api\ExtensibleDataInterface[] $items
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setItems(array $items = null)
    {
        return $this;
    }
    
    /**
     * Retrieve collection items
     *
     * @return \Magento\Framework\DataObject[]
     */
    public function getItems()
    {
    	$select = $this->getSelect();
    	//Add main_table to where part
    	$wherePart = $select->getPart(\Magento\Framework\DB\Select::WHERE);
    	$select->reset(\Magento\Framework\DB\Select::WHERE);
    	foreach($wherePart as $key => $part)
    	{
    		$select->where(str_replace("`entity_id`", "main_table.entity_id", $part));
    
    	}
    	//echo $select->__toString(); exit();
    
    	return parent::getItems();
    }
}
