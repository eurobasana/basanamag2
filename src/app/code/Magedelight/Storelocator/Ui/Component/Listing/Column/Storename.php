<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Ui\Component\Listing\Column;

class Storename extends \Magento\Ui\Component\Listing\Columns\Column
{

    const NAME = 'pickup_store';

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return void
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');

            foreach ($dataSource['data']['items'] as & $item) {
                if (!empty($item['pickup_store'])) {
                    $_storeid = intval($item['pickup_store']);
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $store = $objectManager->get('Magedelight\Storelocator\Model\Storelocator')->load($_storeid);
                    
                    $item[$fieldName] = $store->getStorename();
                } else {
                    $item[$fieldName] ='';
                }
            }
        }
        return $dataSource;
    }
}
