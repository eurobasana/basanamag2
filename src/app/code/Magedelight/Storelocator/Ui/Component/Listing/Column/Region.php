<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Ui\Component\Listing\Column;

class Region extends \Magento\Ui\Component\Listing\Columns\Column
{

    const NAME = 'state';

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');

            foreach ($dataSource['data']['items'] as & $item) {
                if (!empty($item['state'])) {
                    $item[$fieldName] = $item['state'];
                } elseif (isset($item['region_id']) && $item['region_id'] != 0) {
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $region = $objectManager->get('Magento\Directory\Model\Region')->load($item['region_id']);
                    $item[$fieldName] = $region->getName();
                }
            }
        }
        return $dataSource;
    }
}
