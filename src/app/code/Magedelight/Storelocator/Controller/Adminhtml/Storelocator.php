<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magedelight\Storelocator\Model\StorelocatorFactory;
use Magento\Framework\Registry;

abstract class Storelocator extends Action
{

    /**
     * storelocator factory
     *
     * @var AuthorFactory
     */
    protected $storelocatorFactory;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    
    /**
     * date filter
     *
     * @var \Magento\Framework\Stdlib\DateTime\Filter\Date
     */
    protected $dateFilter;

    /**
     * @param Registry $registry
     * @param AuthorFactory $storelocatorFactory
     * @param Context $context
     */
    public function __construct(
        Registry $registry,
        StorelocatorFactory $storelocatorFactory,
        Context $context
    ) {
        $this->coreRegistry = $registry;
        $this->storelocatorFactory = $storelocatorFactory;
        $this->resultRedirectFactory = $context->getRedirect();
        parent::__construct($context);
    }

    /**
     *
     * @return object Magedelight\Storelocator\Model\Storelocator
     */
    protected function initStorelocator()
    {
        $storelocatorId = (int) $this->getRequest()->getParam('storelocator_id');
        /** @var \Magedelight\Storelocator\Model\Storelocator $storelocator */
        $storelocator = $this->storelocatorFactory->create();
        if ($storelocatorId) {
            $storelocator->load($storelocatorId);
        }
        $this->coreRegistry->register('magedelight_storelocator_storelocator', $storelocator);
        return $storelocator;
    }
}
