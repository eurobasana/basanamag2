<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Controller\Adminhtml\Storeinfo;

use Magento\Framework\Registry;
use Magedelight\Storelocator\Controller\Adminhtml\Storelocator;
use Magento\Framework\Stdlib\DateTime\Filter\Date;
use Magedelight\Storelocator\Model\StorelocatorFactory;
use Magento\Backend\Model\Session;
use Magento\Backend\App\Action\Context;
use Magedelight\Storelocator\Model\Storelocator\Image as ImageModel;
use Magento\Framework\Exception\LocalizedException;
use Magedelight\Storelocator\Model\Upload;
use Magento\Backend\Helper\Js as JsHelper;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends Storelocator
{

    /**
     * storelocator factory
     * @var
     */
    protected $storelocatorFactory;

    /**
     * image model
     *
     * @var \Magedelight\Storelocator\Model\Storelocator\Image
     */
    protected $imageModel;

    /**
     * file model
     *
     * @var \Magedelight\Storelocator\Model\Storelocator\File
     */
    protected $fileModel;

    /**
     * upload model
     *
     * @var \Magedelight\Storelocator\Model\Upload
     */
    protected $uploadModel;

    /**
     * @var \Magento\Backend\Helper\Js
     */
    protected $jsHelper;

    /**
     *
     * @param JsHelper $jsHelper
     * @param Registry $registry
     * @param ImageModel $imageModel
     * @param Upload $uploadModel
     * @param StorelocatorFactory $storelocatorFactory
     * @param Context $context
     */
    public function __construct(
        JsHelper $jsHelper,
        Registry $registry,
        ImageModel $imageModel,
        Upload $uploadModel,
        StorelocatorFactory $storelocatorFactory,
        Context $context
    ) {
        $this->jsHelper = $jsHelper;
        $this->imageModel = $imageModel;
        $this->uploadModel = $uploadModel;
        parent::__construct($registry, $storelocatorFactory, $context);
    }

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $data = $this->getRequest()->getPost('storelocator');
        
        $timedata = $this->getRequest()->getPost();
        $resultRedirect = $this->resultRedirectFactory->create();
        
        
        if (count($data['store_ids']) >= 1) {
            $data['store_ids'] = implode(',', $data['store_ids']);
        }
        
        if (count($timedata['telephone']) > 1) {
            $data["telephone"] = implode(':', $timedata['telephone']);
        } else {
            $data["telephone"] = $timedata['telephone'][0];
        }

        if (!empty($timedata['storelocator']['address'][1])) {
            $data["address"] = implode('\n', $timedata['storelocator']['address']);
        } else {
            $data["address"] = $timedata['storelocator']['address'][0];
        }

        if (!empty($timedata['storetime'])) {
            $storetime = $timedata['storetime'];

            foreach ($timedata['storetime'] as $key => $value) {
                if ($value['delete'] == 1) {
                    unset($storetime[$key]);
                }
            }
            $data["storetime"] = serialize($storetime);
        }


        if ($data) {
            $storelocator = $this->initStorelocator();
            $storelocator->setData($data);
            try {
                $storeimage = $this->uploadModel->uploadFileAndGetName('storeimage', $this->imageModel->getBaseDir(), $data);
                $storelocator->setStoreimage($storeimage);
                $storelocator->save();
                $this->messageManager->addSuccess(__('The store has been saved.'));
                $this->_getSession()->setMagedelightStorelocatorStorelocatorData(false);

                if ($this->getRequest()->getParam('back')) {
                    $resultRedirect->setPath(
                        '*/*/edit',
                        [
                        'storelocator_id' => $storelocator->getId(),
                        '_current' => true
                            ]
                    );
                    return $resultRedirect;
                }
                $resultRedirect->setPath('*/*/');
                return $resultRedirect;
            } catch (\Magento\Framework\Model\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the store.'));
            }

            $this->_getSession()->setMagedelightStorelocatorStorelocatorData($data);
            $resultRedirect->setPath(
                '*/*/edit',
                [
                'storelocator_id' => $storelocator->getId(),
                '_current' => true
                    ]
            );
            return $resultRedirect;
        }
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magedelight_Storelocator::storeinfo_save');
    }
}
