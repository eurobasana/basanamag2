<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Controller\Adminhtml\Storeinfo;

use Magedelight\Storelocator\Controller\Adminhtml\Storelocator;

class Delete extends Storelocator
{

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('storelocator_id');
        if ($id) {
            $name = "";
            try {
                /** @var \Magedelight\Storelocator\Model\Storelocator $storelocator */
                $storelocator = $this->storelocatorFactory->create();
                $storelocator->load($id);
                $name = $storelocator->getName();
                $storelocator->delete();

                $this->messageManager->addSuccess(__('The store has been deleted.'));
                $this->_eventManager->dispatch(
                    'adminhtml_magedelight_storelocator_storelocator_on_delete',
                    ['name' => $name, 'status' => 'success']
                );
                $resultRedirect->setPath('*/*/');

                return $resultRedirect;
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_magedelight_storelocator_storelocator_on_delete',
                    ['name' => $name, 'status' => 'fail']
                );
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                $resultRedirect->setPath('*/*/edit', ['storelocator_id' => $id]);
                return $resultRedirect;
            }
        }

        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magedelight_Storelocator::storeinfo_save');
    }
}
