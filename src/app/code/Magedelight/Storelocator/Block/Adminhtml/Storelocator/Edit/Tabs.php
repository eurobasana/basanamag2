<?php

/**
* Magedelight
* Copyright (C) 2017 Magedelight <info@magedelight.com>
*
* @category Magedelight
* @package Magedelight_Storelocator
* @copyright Copyright (c) 2017 Mage Delight (http://www.magedelight.com/)
* @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
* @author Magedelight <info@magedelight.com>
*/

namespace Magedelight\Storelocator\Block\Adminhtml\Storelocator\Edit;

use Magento\Backend\Block\Widget\Tabs as WidgetTabs;

class Tabs extends WidgetTabs
{

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('storelocator_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Store Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab(
            'time_info',
            [
            'label' => __('Time Schedule'),
            'title' => __('Time Schedule'),
            'content' => $this->getLayout()->createBlock(
                'Magedelight\Storelocator\Block\Adminhtml\Storelocator\Edit\Tab\Storetimes'
            )->toHtml(),
                ]
        );
        $this->addTab(
            'meta_info',
            [
            'label' => __('Meta Information'),
            'title' => __('Meta Information'),
            'content' => $this->getLayout()->createBlock(
                'Magedelight\Storelocator\Block\Adminhtml\Storelocator\Edit\Tab\Meta'
            )->toHtml(),
                ]
        );
        return parent::_beforeToHtml();
    }
}
