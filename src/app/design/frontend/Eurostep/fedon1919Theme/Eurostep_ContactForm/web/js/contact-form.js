/**
 * Created by sergio on 19/02/18.
 */
define([
    'uiComponent',
    'jquery',
    'ko',
    'mage/translate',
    'mage/validation',
    'jquery/ui'
], function (Component, $, ko, $t) {
    'use strict';

    return Component.extend({

        initialize: function () {
            this._super();

            this.isFormVisible = ko.observable(true);
            this.isSuccessMessage = ko.observable(false);
            this.isErrorMessage = ko.observable(false);
            this.privacyVisible = ko.observable(false);
            this.orderNumberRequest = ko.observable(false);

            this.form = $('#contact-form');

            this.acceptedTerms = ko.observable(false);
            $('body').trigger('processStop');
        },

        showHidePrivacy: function (self) {
            self.privacyVisible() ? self.privacyVisible(false) : self.privacyVisible(true);
        },

        checkTypeIssue: function (data, event) {
            var typeIssue = event.currentTarget.value;
            if(data.reasonsDependencies.indexOf(typeIssue) !== -1){
                data.orderNumberRequest(true);
            }else {
                data.orderNumberRequest(false);
            }
        },

        validateFormData: function () {
            if(this.isFormValid()){
                this.submitForm();
            }
        },

        isFormValid: function () {
            return this.form.validation() && this.form.validation('isValid');
        },

        prepareFormData: function () {
            var self = this,
                processedData = {},
                formData = $(self.form).serializeArray();

            for(var i = 0; i < formData.length; i++){
                // check if is not a spam bot
                if(formData[i].name === 'spmchk' && formData[i].value !== ''){
                    return false;
                }

                // remove unnecessary data
                if(formData[i].name === 'ordernumber' && formData[i].value === '' ||
                    formData[i].name === 'spmchk' && formData[i].value === '' ||
                    formData[i].name === 'accept_terms') continue;

                processedData[formData[i].name] = formData[i].value;
            }

            return processedData;
        },

        submitForm: function () {
            var self = this,
                url = this.actionUrl;
            var param = '';
            if(this.isDefaultController === "true"){
                param = '?ajax=1';
            }
            var data = this.prepareFormData();
            if(data){
                $('body').trigger('processStart');
                $.ajax({
                    url: url + param,
                    data: data,
                    type: 'post',
                    dataType: 'json',

                    success: function(result) {
                        $('body').trigger('processStop');
                        if(result.executed){
                            window.location.href = self.successPage;
                        }else{
                            self.isFormVisible(false);
                            self.isErrorMessage(true);
                            console.log(result);
                        }
                    },
                    error: function (result) {
                        $('body').trigger('processStop');
                        self.isFormVisible(false);
                        self.isErrorMessage(true);
                        console.log(result);
                    }
                });
            }else{
                $('body').trigger('processStop');
                window.location.reload();
            }
        }

    });
});