/**
 * Created by sergio on 19/02/18.
 */
define([
    'uiComponent',
    'jquery',
    'ko',
    'mage/translate',
    'mage/validation',
    'jquery/ui',
    'mage/cookies'
], function (Component, $, ko, $t) {
    'use strict';

    ko.bindingHandlers['datepicker'] = {
        'init': function(element, valueAccessor, allBindingsAccessor) {
            /* Initialize datepicker with some optional options */
            var options = allBindingsAccessor().datePickeroptions || {},
                prop = valueAccessor(),
                $elem = $(element);

            prop($elem.val());

            $elem.datepicker(options);

            /* Handle the field changing */
            ko.utils.registerEventHandler(element, "change", function () {
                prop($elem.datepicker("getDate"));
            });

            /* Handle disposal (if KO removes by the template binding) */
            ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
                $elem.datepicker("destroy");
            });
        },
        'update': function(element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor()),
                $elem = $(element),
                current = $elem.datepicker("getDate");

            if (value - current !== 0) {
                $elem.datepicker("setDate", value);
            }
        }
    };

    return Component.extend({
        // todo refactor validation
        initialize: function () {
            this._super();
            this.userBirthday = ko.observable(null);
            this.privacyVisible = ko.observable(false);
            this.displayMessage = ko.observable('Newsletter');
            this.isUnexpectedError = ko.observable(false);
            this.unexpectedErrorMessage = ko.observable($t('Please reload the page and try again.'));

            this.newsletterData = {
                gender: ko.observable('male'),
                firstname: ko.observable(),
                lastname: ko.observable(),
                email: ko.observable(),
                telephone: ko.observable(),
                userDob: ko.observable()
            };
            this.isFormVisible = ko.observable(true);
            this.acceptedTerms = ko.observable(false);

            var urlParams = new URLSearchParams(window.location.search);
            this.newsletterData.email(urlParams.get('email'));
            $('body').trigger('processStop');
        },

        showHidePrivacy: function (self) {
            self.privacyVisible() ? self.privacyVisible(false) : self.privacyVisible(true);
        },

        checkAge: function () {
            var birthday = +new Date(this.userBirthday());
            return~~ ((Date.now() - birthday) / (31557600000)) > 15;
        },

        validateFormData: function () {
            var self = this;
            var obj = this.newsletterData;
            this.validateBirthDay(this);
            for (var key in obj) {
                if(key !== 'telephone'){
                    if (obj[key]() === undefined || obj[key]() === "" || obj[key]() === null){
                        $('#EC-newsletter-' + key + '-error').text($t('This is a required field.'));
                        $('#EC-newsletter-' + key).addClass('ec-invalid');
                        obj[key].isValid = false;
                    }else{
                        $('#EC-newsletter-' + key + '-error').text('');
                        $('#EC-newsletter-' + key).removeClass('ec-invalid');
                        obj[key].isValid = true;
                    }
                }else if(key === 'telephone' && self.isPhoneRequired === 'req'){
                    if (obj[key]() === undefined || obj[key]() === "" || obj[key]() === null){
                        $('#EC-newsletter-' + key + '-error').text($t('This is a required field.'));
                        $('#EC-newsletter-' + key).addClass('ec-invalid');
                        obj[key].isValid = false;
                    }else{
                        $('#EC-newsletter-' + key + '-error').text('');
                        $('#EC-newsletter-' + key).removeClass('ec-invalid');
                        obj[key].isValid = true;
                    }
                }
            }
            if($('#amgdpr_agree').prop('checked')){
                $('#EC-newsletter-acceptedterms-error').text('');
                self.acceptedTerms.isValid = true;
            }else{
                self.acceptedTerms.isValid = false;
                $('#EC-newsletter-acceptedterms-error').text($t('This is a required field.'));
            }
        },

        validateBirthDay: function (data) {
            if(data.userBirthday() !== null && data.userBirthday() !== ""){
                if(data.checkAge()){
                    var birthDay = new Date(data.userBirthday());
                    data.newsletterData.userDob(birthDay);
                    data.userBirthday.isValid = true;
                    $('#EC-newsletter-birthday-error').text('');
                    $('#EC-newsletter-birthday').removeClass('ec-invalid');
                }else {
                    $('#EC-newsletter-birthday-error').text($t('You must be 16 years of age or older to register on our website'));
                    $('#EC-newsletter-birthday').addClass('ec-invalid');
                    data.userBirthday.isValid = false;
                }
            }
            else{
                $('#EC-newsletter-birthday-error').text($t('This is a required field.'));
                $('#EC-newsletter-birthday').addClass('ec-invalid');
                data.userBirthday.isValid = false;
            }
        },

        isFormValid: function () {
            var self = this;
            this.validateFormData();
            return(
               self.newsletterData.firstname.isValid &&
               self.newsletterData.lastname.isValid &&
               self.newsletterData.email.isValid &&
               self.newsletterData.gender.isValid &&
               self.userBirthday.isValid &&
               self.acceptedTerms.isValid
            )
        },

        submitSubscriptionForm: function () {
            var self = this;
            var spmChk = $('#spmchk').val();
            if(this.isFormValid() && spmChk === ''){
                $('body').trigger('processStart');
                var data = JSON.parse(ko.toJSON(this.newsletterData));
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: self.actionUrl + '?ajax=1',
                    data: data,
                    success: function(result) {
                        $('body').trigger('processStop');
                        if(result.executed === true){
                            //self.isFormVisible(false);
                            //self.displayMessage(result.message);
                            window.location.href = result['redirectUrl'];
                        }else {
                            var emailErr = $('#EC-newsletter-email-error');
                            if(result.error === 'invalid' || result.error === 'subscribed'){
                                emailErr.html(result.message);
                                setTimeout(function () {
                                    emailErr.html('');
                                }, 8000)
                            }else{
                                self.isFormVisible(false);
                                self.isUnexpectedError(true);
                                self.displayMessage(result.message);
                            }
                        }
                    },
                    error: function (result) {
                        $('body').trigger('processStop');
                        self.isFormVisible(false);
                        self.isUnexpectedError(true);
                        console.log('Subscription error: -> ');
                        console.log(result);
                    }
                });
            }
        }

    });
});