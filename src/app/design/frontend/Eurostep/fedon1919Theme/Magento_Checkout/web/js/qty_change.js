/**
 * Created by sergiu on 11/04/18.
 */
define([
    'ko',
    'uiComponent',
    "jquery"
], function (ko, Component, $) {
    'use strict';

    return Component.extend({
        initialize: function () {
            this._super();
        },

        decreaseQty: function(element, event) {
            var input = $(event.currentTarget).siblings('input.qty'),
                curentQty = input.data('current-qty');

            var newQty = parseInt(input.val()) - 1;

            if (newQty < 1) {
                newQty = 1;
            }

            input.val(newQty);

            var button = $('#ec-update-shopping-cart');
            if(parseInt(input.val()) !== parseInt(curentQty)){
                button.addClass('ec-cta-active');
            }else{
                button.removeClass('ec-cta-active');
            }
        },

        increaseQty: function(element, event) {
            var input = $(event.currentTarget).siblings('input.qty'),
                curentQty = input.data('current-qty');

            var newQty = parseInt(input.val()) + 1;

            input.val(newQty);

            var button = $('#ec-update-shopping-cart');
            if(parseInt(input.val()) !== parseInt(curentQty)){
                button.addClass('ec-cta-active');
            }else{
                button.removeClass('ec-cta-active');
            }
        }
    });
});