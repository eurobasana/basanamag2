require([
        'jquery',
        'jquery/ui',
        'matchMedia'
    ],

    /* activate menu tabs in Legal pages */
    function($, tabs) {

        /* script for previous legal pages funcionality -> one page tabs */

        /* $("#legal-area-tabs").tabs();*/

        /* mobile menu select in Legal Area */

        /* $( "#legal-area-tabs-menu-mobile" ).change(function() {

            hideAllDivs = function () {
                $("#legal-area-tabs > .legal-area-tabs-content").hide();
            };

            handleNewSelection = function () {
                hideAllDivs();

                var serviceClass = $(this).val();
                var serviceId = '#'+serviceClass;
                $(serviceId).show();

            };

            $("#legal-area-tabs-menu-mobile").change(handleNewSelection);

            // Run the event handler once now to ensure everything is as it should be
            handleNewSelection.apply($("#legal-area-tabs-menu-mobile"));

        });*/


        /*  legal area mobile menu functionality */

        var updateSelectedValue = function() {
            var legalarea_visible_content = $('.legal-area-tabs-content').attr('id');

            if ($('#'+legalarea_visible_content).length){
                var legalarea_menu_option_val = legalarea_visible_content;

                var legalarea_menu_option_selected =  $('#legal-area-tabs-menu-mobile option[value="' + legalarea_menu_option_val + '"]');

                legalarea_menu_option_selected.prop('selected', true);
            }
        }

        window.setTimeout(updateSelectedValue, 2000);


        /*  legal area active page  */

        var legalarea_visible_content = $('.legal-area-tabs-content').attr('id');
        $('#legal-area-tabs-menu__' + legalarea_visible_content).addClass( "ui-tabs-active");



        //Footer accordion

        var $footerAccordion = $('.footer-acc');

        mediaCheck({
            media: '(min-width: 768px)',
            entry: function () {
                if ($footerAccordion.data('mageAccordion') != null)
                    $footerAccordion.accordion('destroy');
            },
            exit: function () {

                //Attivo l'accordion del footer
                $footerAccordion.accordion({
                    header:  '[data-role=collapsible]',
                    content: '[data-role=content]',
                    active: true,
                    collapsible: true,
                    multipleCollapsible: false,
                    closedState: 'close',
                    openedState: 'open',
                    animate: 250
                });

            }
        });


        // Account area, fix add comment function into RMA, enable Submit button only when comment textarea is not empty */

        var rma_comment_submit_button = $('.amasty-rma-request-view .buttons-set .action.primary');

        rma_comment_submit_button.attr('disabled', true);

        $('.amasty-rma-request-view #rma_comment_text').on('keyup',function() {
            rma_comment_submit_button.attr('disabled', false);
        });



    }

);
