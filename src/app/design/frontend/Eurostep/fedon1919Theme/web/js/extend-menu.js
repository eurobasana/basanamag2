/**
 * Created by sergiu on 06/06/18.
 */
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'matchMedia',
    'jquery/ui',
    'mage/menu'
], function ($, mediaCheck) {
    'use strict';

    /**
     * Menu Widget - this widget is a wrapper for the jQuery UI Menu
     */
    $.widget('fedon.menu', $.mage.menu, {

        options: {
            mediaBreakpoint: '(max-width: 1022px)'
        },

        /**
         * @private
         */
        _init: function () {
            this._super();

            if (this.options.responsive === true) {
                mediaCheck({
                    media: this.options.mediaBreakpoint,
                    entry: $.proxy(function () {
                        this._addViewAll();
                    }, this),
                    exit: $.proxy(function () {
                        //this._toggleDesktopMode();
                    }, this)
                });
            }
        },

        /**
         * @private
         */
        _addViewAll: function () {
            var subMenus;

            subMenus = this.element.find('li.level-top > ul > li');
            $.each(subMenus, $.proxy(function (index, item) {
                var categoryUrl = $(item).find('> a').attr('href'),
                    menu = $(item).find('> ul.ui-menu');

                this.categoryLink = $('<a>').attr('href', categoryUrl).text($.mage.__('View all'));

                this.categoryParent = $('<li>').addClass('ui-menu-item ec-all-category').html(this.categoryLink);

                if (menu.find('.ec-all-category').length === 0) {
                    menu.append(this.categoryParent);
                }

            }, this));
        },

        /**
         * @param {*} submenu
         * @private
         */
        _open: function (submenu) {

            clearTimeout(this.timer);
            this.element.find('.ui-menu').not(submenu.parents('.ui-menu'))
                .hide()
                .attr('aria-hidden', 'true');

            submenu
                .show()
                .removeAttr('aria-hidden')
                .attr('aria-expanded', 'true')
                .css({
                    'top': '105px',
                    'left': '-35px'
                });
            $(submenu).parent().addClass('ec-after-zone');
        },

        _close: function (startMenu) {
            if (!startMenu) {
                startMenu = this.active ? this.active.parent() : this.element;
            }
            startMenu
                .find('.ui-menu')
                .hide()
                .attr('aria-hidden', 'true')
                .attr('aria-expanded', 'false')
                .end()
                .find('a.ui-state-active')
                .removeClass('ui-state-active');

            startMenu
                .find('.ec-after-zone')
                .removeClass('ec-after-zone');
        }


    });

    return $.fedon.menu;

});
